provider "aws" {
  region = var.region

  assume_role {
    role_arn = "arn:aws:iam::${var.aws_organizations_account_resource[var.account_name].id}:role/OrganizationAccountAccessRole"
  }
}

provider "aws" {
  alias  = "audit"
  region = var.region

  assume_role {
    role_arn = "arn:aws:iam::${var.aws_organizations_account_resource["Audit"].id}:role/OrganizationAccountAccessRole"
  }
}
