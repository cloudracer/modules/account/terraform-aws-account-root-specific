locals {
  tags_module = {
    Terraform               = true
    Terraform_Module        = "terraform-aws-account-root-specific"
    Terraform_Module_Source = "https://gitlab.com/tecracer-intern/terraform-landingzone/modules/terraform-aws-account-root-specific"
  }
  tags = merge(local.tags_module, var.tags)
}
