resource "aws_iam_role" "billing" {
  name = "BillingRole"
  assume_role_policy = templatefile("${path.module}/templates/assume_role.tpl", {
    PRINCIPAL_TYPE = "AWS",
    PRINCIPAL      = var.billing_role_principals
  })

  tags = local.tags
}

resource "aws_iam_role_policy_attachment" "billing" {
  role       = aws_iam_role.billing.name
  policy_arn = "arn:aws:iam::aws:policy/job-function/Billing"

  depends_on = [aws_iam_role.billing]
}
